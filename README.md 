# **Yerevan** 
### [Text Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Yerevan)

[![Yerevan Views](https://i.pinimg.com/564x/4a/0e/74/4a0e7428ee9644dc6e6a74af645c57b2.jpg)

**Yerevan** (/ˌjɛrəˈvɑːn/, YE-rə-VAHN; Armenian: Երևան [jɛɾɛˈvɑn] (About this sound listen), sometimes spelled Erevan) is the capital and largest city of Armenia as well as one of the world's oldest continuously inhabited cities. Situated along the Hrazdan River, Yerevan is the administrative, cultural, and industrial center of the country. It has been the capital since 1918, the thirteenth in the history of Armenia, and the seventh located in or around the [Ararat plain](https://en.wikipedia.org/wiki/Ararat_Plain). The city also serves as the seat of the Araratian Pontifical Diocese; the largest diocese of the Armenian Apostolic Church and one of the oldest dioceses in the world.

## Etymology

The *"birth certificate"* of Yerevan at the Erebuni Fortress—a cuneiform inscription left by King Argishti I of Urartu on a basalt stone slab about the foundation of the city in 782 BC

**"YEREVAN" (ԵՐԵՒԱՆ)**in inscription from Kecharis, dating back to 1223.

One theory regarding the origin of Yerevan's name is the city was named after the Armenian king, Yervand (Orontes) IV, the last leader of the Orontid Dynasty, and founder of the city of Yervandashat. However, it is likely that the city's name is derived from the Urartian military fortress of Erebuni (Էրեբունի), which was founded on the territory of modern-day Yerevan in 782 BC by Argishti I. As elements of the Urartian language blended with that of the Armenian one, the name eventually evolved into Yerevan (Erebuni = Erevani = Erevan = Yerevan). Scholar Margarit Israelyan notes these changes when comparing inscriptions found on two cuneiform tablets at [Erebuni](https://en.wikipedia.org/wiki/Erebuni_Fortress):

>> The transcription of the second cuneiform bu [original emphasis] of the word was very essential in our interpretation as it is the Urartaean b that has been shifted to the Armenian v (b > v). The original writing of the inscription read «er-bu-ni»; therefore the prominent Armenianologist-orientalist Prof. G. A. Ghapantsian justly objected, remarking that the Urartu b changed to v at the beginning of the word (Biani > Van) or between two vowels (ebani > avan, Zabaha > Javakhk)....In other words b was placed between two vowels. The true pronunciation of the fortress-city was apparently Erebuny.>> 



###Notes 

- Erebuni Fortress Elevation and Position
 Israelyan, Margarit A (1971). Էրեբունի: Բերդ-Քաղաքի Պատմություն (Erebuni: The History of a Fortress-City) (in Armenian). Yerevan: Hayastan Publishing Press. pp. 8–15.
 Israelyan. Erebuni, pp. 12-13.

 - Jaimoukha, Amjad. The Chechens: A Handbook. Page 29. Available at Google Books: 


####**Other Cities and Information** 

| Details       |  Country      | Pupulation (Total)|
| ------------- |:-------------:| -----------------:|
| Yerevan       | Armenia       | 1,075,800 |
| Gyumri        | Armenia       |  121,976 |
| Vanadzor      | Armenia      |    86,199 |

